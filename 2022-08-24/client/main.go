package main

import (
	pb "bookshop/client/pb/inventory"
	"context"
	"io"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {

	conn, err := grpc.Dial("localhost:8080", grpc.WithTransportCredentials(insecure.NewCredentials()))

	if err != nil {
		log.Fatalf("failed to connect : %v", err)

	}
	defer conn.Close()

	client := pb.NewInventoryClient(conn)
	bookList, err := client.GetBookList(context.Background(), &pb.GetBookListRequest{})

	if err != nil {
		log.Fatalf("failed to get book list: %v", err)
	}

	log.Printf("book list: %v", bookList)

	listenToBookList(client)

}

func listenToBookList(client pb.InventoryClient) {

	stream, err := client.ListenToBookList(context.Background(), &pb.GetBookListRequest{Title: "Doraemon"})
	if err != nil {
		panic(err)
	}

	for {
		output, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("%v.ListFeatures(_) = _, %v", client, err)
		}
		log.Println(output)
	}

}
