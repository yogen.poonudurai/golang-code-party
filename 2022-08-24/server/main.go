package main

import (
	pb "bookshop/server/pb/inventory"
	"context"
	"fmt"
	"log"
	"net"
	"strings"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type server struct {
	pb.UnimplementedInventoryServer
}

func (s *server) GetBookList(ctx context.Context, req *pb.GetBookListRequest) (*pb.GetBookListResponse, error) {
	return &pb.GetBookListResponse{
		Books: getSampleBooks(),
	}, nil
}

func (s *server) ListenToBookList(req *pb.GetBookListRequest, stream pb.Inventory_ListenToBookListServer) error {

	fmt.Println("ListenToBookList called")
	characters := strings.Split(req.Title, "")

	for _, char := range characters {
		fmt.Println(char)
		time.Sleep(time.Second)
		stream.Send(&pb.GetTitles{Books: char})
	}

	return nil
}

func main() {

	listener, err := net.Listen("tcp", ":8080")
	if err != nil {
		panic(err)
	}

	s := grpc.NewServer()
	reflection.Register(s)
	pb.RegisterInventoryServer(s, &server{})

	log.Printf("Server started at : %v", listener.Addr().String())
	if err := s.Serve(listener); err != nil {
		log.Fatalf("failed to server, %v", err)
	}

}

func getSampleBooks() []*pb.Book {
	sampleBooks := []*pb.Book{
		{
			Title:     "The Hitchhiker's Guide to the Galaxy",
			Author:    "Douglas Adams",
			PageCount: 42,
		},
		{
			Title:     "The Lord of the Rings",
			Author:    "J.R.R. Tolkien",
			PageCount: 1234,
		},
	}

	return sampleBooks
}
