package main

import (
	pb "bookshop/client/pb/inventory"
	"context"
	"log"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {

	conn, err := grpc.Dial("localhost:8080", grpc.WithTransportCredentials(insecure.NewCredentials()))

	if err != nil {
		log.Fatalf("failed to connect : %v", err)

	}
	defer conn.Close()

	client := pb.NewInventoryClient(conn)
	// bookList, err := client.GetBookList(context.Background(), &pb.GetBookListRequest{})

	// if err != nil {
	// 	log.Fatalf("failed to get book list: %v", err)
	// }

	// log.Printf("book list: %v", bookList)

	getMultipleBooks(client)

}

func getMultipleBooks(client pb.InventoryClient) {
	books := []pb.GetBookListRequest{
		pb.GetBookListRequest{
			Title: "Chemistry books",
		},
		pb.GetBookListRequest{
			Title: "Mathematics books",
		},
		pb.GetBookListRequest{
			Title: "Biology books",
		},
	}

	stream, err := client.GetMultipleBooks(context.Background())

	if err != nil {
		log.Fatalf("%v getMultipleBooks() %v", client, err)
	}

	for _, book := range books {

		stream.Send(&book)

		time.Sleep(time.Second)
	}

	res, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("CloseAndRecv() error %v", client, err)
	}

	log.Println(res)
}
