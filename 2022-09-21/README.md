<h1>Building a gPRC Server streaming in Go - Part 3</h1>

[Video Link](https://drive.google.com/file/d/1C19yNndkpwE4D5i5DBv2ZHlU3NaQjGPP/view?usp=sharing)
Supporting Materials
- [What is gPRC?](https://grpc.io/docs/what-is-grpc/core-concepts/)


<h1>Scripts</h1>
1. Run `protoc --proto_path=../proto ../proto/*.proto --go_out=. --go-grpc_out=.` in server / client directory in order to generate codes.

