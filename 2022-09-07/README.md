Steps

0. cd into `server` folder and run `go mod tidy`. Then, run `go run main.go`
1. Please make sure pnpm is install by running `npm install -g pnpm`.cd into `frontend` folder and run `pnpm i`. Then, run `pnpm dev` to launch the frontend.
2. Open the console to check if the browser can create websocket connection with the server. Modify the server port if required.
