import { ChangeEvent, FormEvent, useEffect, useRef, useState } from "react";
import "./App.css";
import { connect, sendMessageToServer, socket } from "./api/socket";

function App() {
  const [message, setMessage] = useState("hello");
  const [chatHistory, setChatHistory] = useState<any[]>([""]);
  const [messageCount, setMessageCount] = useState(chatHistory.length || 0);

  const ws = useRef<WebSocket | null>(null);

  useEffect(() => {
    connect(() => {});
  }, []);

  useEffect(() => {
    console.log("message count trigger");
    ws.current = socket;
    ws.current.onmessage = (msg: MessageEvent) => {
      const message = JSON.parse(msg.data);
      setChatHistory([...chatHistory, message]);
    };
  }, [messageCount]);

  useEffect(() => {}, [ws]);

  function send(e: FormEvent) {
    e.preventDefault();
    sendMessageToServer(message);
    setMessageCount(messageCount + 1);
  }

  function handleChange(e: ChangeEvent<HTMLInputElement>) {
    if (e.target.value) {
      setMessage(e.target.value);
    }
  }

  return (
    <div className="App">
      message count: {messageCount}
      {chatHistory?.map((chat, index) => (
        <p key={chat + index}>{chat}</p>
      ))}
      <form onSubmit={send}>
        <input
          className=""
          type="text"
          value={message}
          onChange={handleChange}
        />
        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default App;
