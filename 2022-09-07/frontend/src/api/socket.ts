const socket = new WebSocket("ws://localhost:8080/ws")

let connect =  (callback:Function) => {

	socket.onopen = ()=> {
		console.log("Successfully connected")
	}

	socket.onmessage = (msg) => {
		callback(msg)
	}

	socket.onclose = (event)=> {
		console.log("Socket closed connection: ", event)
	}

	socket.onerror = (error ) =>{
		console.log("Socket Error:", error)
	}
}


function sendMessageToServer (msg:string) {

	if (socket.readyState !== socket.OPEN){
		console.log("socket is not open!")
		return
	}

	socket.send(JSON.stringify(msg))

}

export { connect, sendMessageToServer, socket }